package main

import (
	"log"
	"net/http"
	"path/filepath"
	"strings"
)

// The port deserve by the Web Server
var port = ":8080"

// The base root of the Web Serve
var root = "/document/"

// The local filesystem to deserve files (it can be absolute path)
var localDirectory = filepath.FromSlash("./test/")

func main() {

	http.HandleFunc(root, func(res http.ResponseWriter, req *http.Request) {
		println("==========")
		println("requestURI    : " + req.RequestURI)
		path := filepath.Join(localDirectory, strings.ReplaceAll(strings.ReplaceAll(req.RequestURI, root, ""), "..", ""))

		println("local Path    : " + path)
		symlink, err := filepath.EvalSymlinks(path)
		if err != nil {
			symlink = path
		}
		println("final Path    : " + symlink)

		println("base filename : " + filepath.Base(symlink))
		res.Header().Set("Content-Disposition", "attachment; filename="+filepath.Base(symlink))

		http.ServeFile(res, req, symlink)
	})

	println("Server listen on port " + port)
	log.Fatal(http.ListenAndServe(port, nil))
}
