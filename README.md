# rp-ln

## POC explaination

This project is just a small POC to serve a Go Web Server that provides the original file and filename from the symbolic file that used in the request URI.

We have 2 main original files and 2 symlink files in a test directory.
```bash
└── test
    └── mypath
        └── subpath
            ├── test-ln.pdf -> test.pdf
            ├── test-ln.txt -> test.txt
            ├── test.pdf
            └── test.txt
```

The **main.go** serves a WebServer that provide the original file of a symlink file.

## How to run this POC

Install Go

**Ubuntu**

```bash
$ sudo apt install golang
```

**Windows**

Follow the guide on : https://golang.org/doc/install


## Option 1: Run the server with go

```bash
$ cd <path_of_the_project>
$ go run .
```

## Option 2: Build & Run the final executable

If you want a executable for Linux 64 bits
```bash
env GOOS=linux GOARCH=amd64 go build sebastien-dupire.info/rp-ln
```

and just run 
```bash
./rp-ln
```

If you want a executable for Windows 64 bits
```bash
env GOOS=windows GOARCH=amd64 go build sebastien-dupire.info/rp-ln
```
and just run 
```bash
./rp-ln.exe
```


## Test the different URLs

| original URI | status code | local file
|--|--|--
|http://localhost:8080/document/mypath/subpath/test.pdf  | 200 | *./test/mypath/subpath/test.pdf*
|http://localhost:8080/document/mypath/subpath/test-ln.pdf | 200 | *./test/mypath/subpath/test.pdf*
|http://localhost:8080/mypath/ | 404
|http://localhost:8080/document/bidon | 404


The result in a browser is a force-downloaded file with the original filename (not the one provide in the URL if it is symbolic link)

![](doc/result-with-lnfile.jpg)